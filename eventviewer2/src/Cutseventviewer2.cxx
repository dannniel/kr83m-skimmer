#include "Cutseventviewer2.h"
#include "ConfigSvc.h"
#include "TFile.h"
#include "TCutG.h"

Cutseventviewer2::Cutseventviewer2(EventBase* eventBase) {
  m_event = eventBase;
}

Cutseventviewer2::~Cutseventviewer2() {
  
}

// Function that lists all of the common cuts for this Analysis
bool Cutseventviewer2::eventviewer2CutsOK() {
  // List of common cuts for this analysis into one cut
  return true;
}


bool Cutseventviewer2::eventviewer2ssPureEnergy(){                                                                                                                                            \

  bool yesorno = false;

  float S1area = (*m_event->m_singleScatter)->s1Area_phd;
  if( S1area < 440 && S1area > 100){
    yesorno = true;
  }
  return yesorno;
}



bool Cutseventviewer2::eventviewer2S1Coincidence(){

  bool s1Coincidence = false;
  int s1ID = (*m_event->m_singleScatter)->s1PulseID;
  if (((*m_event->m_tpcPulses)->coincidence)[s1ID] > 3) {
      s1Coincidence = true;
    }
  return s1Coincidence;
}




bool Cutseventviewer2::eventviewer2SingleScatter(){
  bool yesorno = false;
  if ((*m_event->m_singleScatter)->nSingleScatters == 1){
    yesorno=true;
  }
  return yesorno;
}

bool Cutseventviewer2::eventviewer2ssFracWidthTopBand(){
  TFile f("~/ALPACA/run/kr83mBranchLoaded/SquareCut.root");
  TCutG *TopBand;
  TopBand = (TCutG*)f.Get("Square0_100");
  int i=((*m_event->m_singleScatter)->s2PulseID);
  float frac_10_90_Width_ns = ((*m_event->m_tpcPulses)->areaFractionTime90_ns)[i] - ((*m_event->m_tpcPulses)->areaFractionTime10_ns)[i];
  return TopBand->IsInside(((*m_event->m_singleScatter)->driftTime_ns)/1000,frac_10_90_Width_ns);

}

bool Cutseventviewer2::eventviewer2ssFracWidthWeirdBand(){
  TFile f("~/ALPACA/run/mdc3Z/mdc3ZFracWidthBands.root");
  TCutG *BottomBand;
  BottomBand = (TCutG*)f.Get("WeirdEvents700_800");
  int i=((*m_event->m_singleScatter)->s2PulseID);
  float frac_10_90_Width_ns = ((*m_event->m_tpcPulses)->areaFractionTime90_ns)[i] - ((*m_event->m_tpcPulses)->areaFractionTime10_ns)[i];
  return BottomBand->IsInside(((*m_event->m_singleScatter)->driftTime_ns)/1000,frac_10_90_Width_ns);
}
  

bool Cutseventviewer2::eventviewer2ssFracWidthBottomBand(){
  TFile f("~/ALPACA/run/mdc3Z/mdc3ZFracWidthBands.root");
  TCutG *BottomBand;
  BottomBand = (TCutG*)f.Get("BottomBand0_450");
  int i=((*m_event->m_singleScatter)->s2PulseID);
  float frac_10_90_Width_ns = ((*m_event->m_tpcPulses)->areaFractionTime90_ns)[i] - ((*m_event->m_tpcPulses)->areaFractionTime10_ns)[i];
  return BottomBand->IsInside(((*m_event->m_singleScatter)->driftTime_ns)/1000,frac_10_90_Width_ns);

}

bool Cutseventviewer2::eventviewer2ssRadial(){
  bool yesorno = false;
  float radialposition = sqrt(pow((*m_event->m_singleScatter)->x_cm,2) + pow((*m_event->m_singleScatter)->y_cm,2));
  if( radialposition <64){
    yesorno = true;
  }
  return yesorno; 
}
