#include "EventBase.h"
#include "Analysis.h"
#include "HistSvc.h"
#include "Logger.h"
#include "ConfigSvc.h"
#include "SkimSvc.h"
#include "SparseSvc.h"
#include "CutsBase.h"
#include "Cutseventviewer2.h"
#include "eventviewer2.h"
#include <iostream>
#include <fstream>

// SparseSvc* rmsWidth0_2000;
// SparseSvc* rmsWidth2000_5000;
// SparseSvc* rmsWidth5000_10000;

// std::ofstream rmsWidths0_2000_ns;
// std::ofstream rmsWidths2000_5000_ns;
// std::ofstream rmsWidths5000_10000_ns;
// std::ofstream sometestfile;
SparseSvc* TopBandEvents;
SparseSvc* BottomBandEvents;
SparseSvc* WeirdEvents;

std::ofstream TopBandDrifts;
std::ofstream BottomBandDrifts;
std::ofstream WeirdEventDrifts;

std::ofstream TopBandS2;
std::ofstream BottomBandS2;
std::ofstream WeirdEventS2;


// Constructor
eventviewer2::eventviewer2()
  : Analysis()
{
  // Set up the expected event structure, include branches required for analysis.
  // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
  // Load in the Single Scatters Branch
  m_event->IncludeBranch("ss");
  m_event->IncludeBranch("pulsesTPC");
  m_event->Initialize();
  ////////

  // Setup logging
  logging::set_program_name("eventviewer2 Analysis");
  
  // Setup the analysis specific cuts.
  m_cutseventviewer2 = new Cutseventviewer2(m_event);
  
  // Setup config service so it can be used to get config variables
  m_conf = ConfigSvc::Instance();
    
  WeirdEvents = new SparseSvc();
  TopBandEvents = new SparseSvc();
  BottomBandEvents = new SparseSvc();
  // rmsWidth0_2000 = new SparseSvc();
  // rmsWidth2000_5000 = new SparseSvc();
  // rmsWidth5000_10000 = new SparseSvc();


}

// Destructor
eventviewer2::~eventviewer2() {
  delete m_cutseventviewer2;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
// 
// Initialize() -  Called once before the event loop.
void eventviewer2::Initialize() {
  INFO("Initializing eventviewer2 Analysis");
  m_cuts->mdc3()->Initialize();
  // std::string SparsePath = "~/allofit/";
  // std::string OutName = m_conf->OutName;

  // rmsWidth0_2000->InitializeSparse(OutName+".Events_RMSWidth0_2000.txt" );
  // rmsWidth2000_5000->InitializeSparse(OutName+".Events_RMSWidth2000_5000.txt");
  // rmsWidth5000_10000->InitializeSparse(OutName+".Events_RMSWidth5000_10000.txt");

  // m_hists->BookHist("RmsWidths0",100,0,2000);
  // m_hists->BookHist("RmsWidths1",100,2000,5000);
  // m_hists->BookHist("RmsWidths2",100,5000,10000);

  std::string OutName = m_conf->OutName;

  TopBandEvents->InitializeSparse(OutName+".Events_TopBand.txt");
  BottomBandEvents->InitializeSparse(OutName+".Events_BottomBand.txt");
  WeirdEvents->InitializeSparse(OutName+".Event_Weird.txt");

}



  


// Execute() - Called once per event.
void eventviewer2::Execute() {
  if(m_cutseventviewer2->eventviewer2S1Coincidence() && m_cutseventviewer2->eventviewer2SingleScatter() && m_cutseventviewer2->eventviewer2ssRadial()){
     
    std::string OutName = m_conf->OutName;
    if(m_cutseventviewer2->eventviewer2ssFracWidthBottomBand()){
      for(int j=0; j< Number ; j++) {
	double minValue = range0_150[j] - 1;
	double maxValue = range0_150[j] + 1;
	double DriftTime = ((*m_event->m_singleScatter)->driftTime_ns)/1000;
	if(((*m_event->m_singleScatter)->driftTime_ns)/1000 > minValue && ((*m_event->m_singleScatter)->driftTime_ns)/1000 < maxValue){
	  if(indexLoopbin0[j]==0){
	    indexLoopbin0[j] = 1;
	    BottomBandEvents->SparseLog(m_event);
	    using namespace std;
	    BottomBandDrifts.open(OutName+".BottomBandDrifts_us.txt",std::ios_base::app);
	    BottomBandDrifts<<((*m_event->m_singleScatter)->driftTime_ns)/1000<<endl;
	    BottomBandDrifts.close();
	  }
	}
      }
    }
    if(m_cutseventviewer2->eventviewer2ssFracWidthTopBand()){
      TopBandEvents->SparseLog(m_event);
      TopBandDrifts.open(OutName+".TopBandDrifts_us.txt",std::ios_base::app);
      TopBandDrifts<<((*m_event->m_singleScatter)->driftTime_ns)/1000<<endl;
      TopBandDrifts.close();
    }
  } 
}

  // //search for events at the desired points within a range of plus or minus 10. If a hit, don't search in that region anymore by setting the vector entry corresponding to that point to 1.
  // if(m_cutseventviewer2->eventviewer2SingleScatter() && m_cutseventviewer2->eventviewer2ssPureEnergy()){
  //   //identify S2 pulse,
  //   int i = (*m_event->m_singleScatter)->s2PulseID;
    
  //   std::string TextPath = "~/oknotall";
  //   std::string OutName = m_conf->OutName;

  //   //check if the S2 width is within any of the sample point bins.
  //   for(int j=0; j< Number ; j++) {
  //         double minValue = range0_2000[j] - 10;
  //         double maxValue = range0_2000[j] + 10;
  // 	  //if it is within any of these bins, and this sample has yet to be taken from within this sample point bin range, get the skim.
  // 	  if(indexLoopbin0[j]==0){
  // 	    if(((*m_event->m_tpcPulses)->rmsWidth_ns)[i] > minValue && ((*m_event->m_tpcPulses)->rmsWidth_ns)[i] < maxValue){ 
  //             //INFO("0 to 2000 event!");
  // 	      indexLoopbin0[j] = 1;
  //             //sparse
  //             m_hists->GetHistFromMap("RmsWidths0")->Fill(((*m_event->m_tpcPulses)->rmsWidth_ns)[i]);
	     
  // 	      rmsWidths0_2000_ns.open(OutName+".Widths_RMSWidths0_2000_ns.txt",std::ios_base::app);
  // 	      rmsWidths0_2000_ns<<(((*m_event->m_tpcPulses)->rmsWidth_ns)[i])<<endl;
  // 	      rmsWidths0_2000_ns.close();
  // 	      rmsWidth0_2000->SparseLog(m_event);
  // 	    }
  // 	  }
  // 	}                                                                                                                                                                                           
  //   for(int j=0; j< Number ; j++) {
  //         double minValue = range2000_5000[j] - 10;
  //         double maxValue = range2000_5000[j] + 10;
  //         if(indexLoopbin1[j]==0){
  // 	    if( ((*m_event->m_tpcPulses)->rmsWidth_ns)[i] > minValue && ((*m_event->m_tpcPulses)->rmsWidth_ns)[i] < maxValue){
  // 	      // INFO("2000_5000 event!");
  // 	      indexLoopbin1[j] = 1;
  //             //sparse
  // 	      m_hists->GetHistFromMap("RmsWidths1")->Fill(((*m_event->m_tpcPulses)->rmsWidth_ns)[i]);
  // 	      rmsWidths2000_5000_ns.open(OutName+".Widths_RMSWidths2000_5000_ns.txt",std::ios_base::app);
  // 	      rmsWidths2000_5000_ns<<(((*m_event->m_tpcPulses)->rmsWidth_ns)[i])<<endl;
  // 	      rmsWidths2000_5000_ns.close();
  // 	      rmsWidth2000_5000->SparseLog(m_event);
  //           }
  //         }
  // 	}
  //       for(int j=0; j< Number ; j++) {
  //         double minValue = range5000_10000[j] - 40;
  //         double maxValue = range5000_10000[j] + 40;
  // 	  if(indexLoopbin2[j]<5){
  // 	    if( ((*m_event->m_tpcPulses)->rmsWidth_ns)[i] > minValue && ((*m_event->m_tpcPulses)->rmsWidth_ns)[i] < maxValue){
  // 	      // INFO("5000 to 10000 event!");
  // 	      indexLoopbin2[j]++;
  // 	      //sparse
  // 	      m_hists->GetHistFromMap("RmsWidths2")->Fill(((*m_event->m_tpcPulses)->rmsWidth_ns)[i]);
  // 	      rmsWidths5000_10000_ns.open(OutName+".Widths_RMSWidths5000_10000_ns.txt",std::ios_base::app);
  // 	      rmsWidths5000_10000_ns<<(((*m_event->m_tpcPulses)->rmsWidth_ns)[i])<<endl;
  // 	      rmsWidths5000_10000_ns.close();
  // 	      rmsWidth5000_10000->SparseLog(m_event);
  // 	    }
  // 	  }
  // 	}
  // }



// Finalize() - Called once after event loop.
void eventviewer2::Finalize() {
  INFO("Finalizing eventviewer2 Analysis");
  // delete rmsWidth0_2000;
  // delete rmsWidth2000_5000;
  // delete rmsWidth5000_10000;
  delete TopBandEvents;
  delete BottomBandEvents;
}

