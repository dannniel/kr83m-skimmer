#ifndef Cutseventviewer2_H
#define Cutseventviewer2_H

#include "EventBase.h"

class Cutseventviewer2  {

public:
  Cutseventviewer2(EventBase* eventBase);
  ~Cutseventviewer2();
  bool eventviewer2CutsOK();
  bool eventviewer2ssPureEnergy();
  bool eventviewer2SingleScatter();
  bool eventviewer2ssRadial();
  bool eventviewer2ssFracWidthTopBand();
  bool eventviewer2ssFracWidthBottomBand();
  bool eventviewer2S1Coincidence();
  bool eventviewer2ssFracWidthWeirdBand();

private:
  
  EventBase* m_event;

};

#endif
