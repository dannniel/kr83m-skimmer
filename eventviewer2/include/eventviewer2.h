#ifndef eventviewer2_H
#define eventviewer2_H

#include "Analysis.h"

#include "EventBase.h"
#include "Cutseventviewer2.h"

#include <TTreeReader.h>
#include <string>

class eventviewer2: public Analysis {

public:
  eventviewer2(); 
  ~eventviewer2();

  void Initialize();
  void Execute();
  void Finalize();

 private:
  
  double rmsWidthMinbin0=10;
  double rmsWidthMaxbin0=1990;
  double rmsWidthMinbin1=2000;
  double rmsWidthMaxbin1=4990;
  double rmsWidthMinbin2=5000;
  double rmsWidthMaxbin2=10000;
  int Number = 30;
  double DriftMin=1.5;
  double DriftMax=150;
  
  //A vector which to limit the number of events sampled to 10
  vector<int> indexLoopbin0 = vector<int>(Number, 0);
  vector<int> indexLoopbin1 = vector<int>(Number, 0); 
  vector<int> indexLoopbin2= vector<int>(Number, 0);
  
  
  //lnspace function to determine sample points
  vector<double> lnspace(double min, double max, int N) {
    vector<double> lnspaced;
    double delta = (max-min)/double(N-1);
    for(int i=0; i<N; i++) {
      lnspaced.push_back(min + i*delta);
    }
    return lnspaced;
  }
  
  /* vector<double> range0_2000 = lnspace(rmsWidthMinbin0,rmsWidthMaxbin0,Number); */
  /* vector<double> range2000_5000 = lnspace(rmsWidthMinbin1,rmsWidthMaxbin1,Number); */
  /* vector<double> range5000_10000 = lnspace(rmsWidthMinbin2,rmsWidthMaxbin2,Number); */
  vector<double> range0_150 = lnspace(DriftMin,DriftMax,Number);


protected:
  Cutseventviewer2* m_cutseventviewer2;
  ConfigSvc* m_conf;
};

#endif
